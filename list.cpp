﻿#include <iostream>
#include "list.h"
using namespace std;
	
void List::headPush(int var) {

	if (tail == 0 && head == 0) { //fist push
		head = new Node(var, head); // Don't need tmp ;)
		tail = head;
	} else {
		head = new Node(var, head);
	}

}

void List::tailPush(int var) {

	Node *tmp = head;

	while (true) { // forever loop 

		if (tmp->next == 0) { // find last element

			if (tail == 0 && head == 0) { //fist push
				tail = new Node(var, 0);
				tmp->next = tail;

				head = tail;

			} else {
				tail = new Node(var, 0);
				tmp->next = tail;
			}

			return;
		} else {
			tmp = tmp->next;
		}
	}

}

int List::headPop() {

	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}

	Node *tmp = head->next;

	delete head;

	head = tmp;

	return head->info;

}

int List::tailPop() {
	
	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}
	
	Node *tmp = head;

	while (true) {
		
		if (tmp->next->next == 0) { // find last elemnet
		
			tail = tmp; //  copy tmp to tail
			tail->next = 0; //set next to Null

			tmp = tmp->next; // set tmp to last element

			delete tmp; // delete last element
			
			return tail->info; // return 
		} else {
			tmp = tmp->next;
		}
	}

	
}

bool List::isInList(int search) {

	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}

	Node *tmp = head;

	cout << "Serach : " << search << " IN( ";

	while (true) {
		
		cout << " " << tmp->info << " ";

		if (tmp->info == search) {

			cout << " )";

			return true; // return true if found
		}

		tmp = tmp->next; // next pointer
	
		if (tmp == 0) {

			cout << " )";

			return false; // return false
		}

	}

}

void List::deleteNode(int search){

	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return;
	}

	Node *tmp = head; // pointer
	Node *tmp_2 = head; // pointer

	while (true) {

		if (tmp->info == search) {
			if (tmp == tail) { // check last element
				List::tailPop(); //tailpop ;)
				return;
			} else if(tmp == head) {
				List::headPop(); //headpop ;)
				return;
			} else {
			
				tmp_2->next= tmp->next;  // tmp_2 -> delete here(tmp) -> skip

				delete tmp;  // delete tmp

				tmp = tmp_2;  // tmp_2 -> skip

				return;
			
			}
		}

		if (tmp != head) {
			tmp_2 = tmp_2->next; // use for delete not head or tail node
		}

		tmp = tmp->next;

		if (tmp == 0) return;

	}
}

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}